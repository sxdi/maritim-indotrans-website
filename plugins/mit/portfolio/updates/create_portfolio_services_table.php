<?php namespace Mit\Portfolio\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePortfolioServicesTable extends Migration
{
    public function up()
    {
        Schema::create('mit_portfolio_portfolio_services', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('portfolio_id');
            $table->integer('service_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mit_portfolio_portfolio_services');
    }
}
