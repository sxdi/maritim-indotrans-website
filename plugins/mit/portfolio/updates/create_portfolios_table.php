<?php namespace Mit\Portfolio\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePortfoliosTable extends Migration
{
    public function up()
    {
        Schema::create('mit_portfolio_portfolios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('client_id');
            $table->string('title');
            $table->text('body');
            $table->timestamps();
            $table->string('slug');
        });
    }

    public function down()
    {
        Schema::dropIfExists('mit_portfolio_portfolios');
    }
}
