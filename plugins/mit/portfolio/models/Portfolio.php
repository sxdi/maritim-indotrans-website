<?php namespace Mit\Portfolio\Models;

use Model;

/**
 * Portfolio Model
 */
class Portfolio extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement    = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['title', 'location', 'body'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mit_portfolio_portfolios';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne         = [];
    public $hasMany        = [];
    public $hasOneThrough  = [];
    public $hasManyThrough = [];
    public $belongsTo      = [
        'client' => [
            'Mit\Client\Models\Client',
            'key'      => 'client_id',
            'otherKEy' => 'id'
        ]
    ];
    public $belongsToMany  = [
        'services' => [
            'Mit\Service\Models\Service',
            'table'    => 'mit_portfolio_portfolio_services',
            'key'      => 'portfolio_id',
            'otherKey' => 'service_id'
        ]
    ];
    public $morphTo        = [];
    public $morphOne       = [];
    public $morphMany      = [];
    public $attachOne      = [];
    public $attachMany     = [
        'galleries' => ['System\Models\File']
    ];
}
