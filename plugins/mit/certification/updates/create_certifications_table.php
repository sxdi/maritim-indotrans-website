<?php namespace Mit\Certification\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCertificationsTable extends Migration
{
    public function up()
    {
        Schema::create('mit_certification_certifications', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code');
            $table->string('number');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mit_certification_certifications');
    }
}
