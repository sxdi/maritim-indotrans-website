<?php namespace Mit\Setting;

use Backend;
use System\Classes\PluginBase;

/**
 * Setting Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Setting',
            'description' => 'No description provided yet...',
            'author'      => 'Mit',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Event::listen('cms.page.beforeDisplay', function($controller, $page, $url) {
            $controller->vars['settings'] = [
                'appearance' => \Mit\Setting\Models\Appearance::instance(),
                'site'       => \Mit\Setting\Models\Site::instance(),
                'page'       => \Mit\Setting\Models\Page::instance(),
            ];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Mit\Setting\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'mit.setting.some_permission' => [
                'tab' => 'Setting',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'setting' => [
                'label'       => 'Setting',
                'url'         => Backend::url('mit/setting/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['mit.setting.*'],
                'order'       => 500,
            ],
        ];
    }

    /**
     * [registerSettings description]
     * @return [type] [description]
     */
    public function registerSettings()
    {
        return [
            'website' => [
                'label'       => 'Website',
                'description' => 'Manage available website for maritim indo trans website',
                'category'    => 'Site',
                'icon'        => 'icon-globe',
                'class'       => 'Mit\Setting\Models\Site',
                'order'       => 100,
            ],
            'page' => [
                'label'       => 'Page',
                'description' => 'Manage available page for maritim indo trans website',
                'category'    => 'Site',
                'icon'        => 'icon-file',
                'class'       => 'Mit\Setting\Models\Page',
                'order'       => 101,
            ],
            'appearance' => [
                'label'       => 'Appearance',
                'description' => 'Manage available appearance for maritim indo trans website',
                'category'    => 'Site',
                'icon'        => 'icon-image',
                'class'       => 'Mit\Setting\Models\Appearance',
                'order'       => 102,
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'ucwords' => [$this, 'makeUcwords']
            ],
        ];
    }

    public function makeUcwords($text)
    {
        return ucwords(strtolower($text));
    }
}
