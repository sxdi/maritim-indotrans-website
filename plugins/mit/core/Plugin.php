<?php namespace Mit\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'Mit',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Mit\Core\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'mit.core.some_permission' => [
                'tab' => 'Core',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate
        return [
            'core' => [
                'label'       => 'MIT',
                'url'         => Backend::url('mit/core/portfolios'),
                'icon'        => 'icon-ship',
                'permissions' => ['mit.core.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'portfolios' => [
                        'label'       => 'Portfolio',
                        'url'         => Backend::url('mit/core/portfolios'),
                        'icon'        => 'icon-briefcase',
                        'permissions' => ['mit.core.*'],
                        'order'       => 500,
                    ],
                    'services' => [
                        'label'       => 'Service',
                        'url'         => Backend::url('mit/core/services'),
                        'icon'        => 'icon-cube',
                        'permissions' => ['mit.core.*'],
                        'order'       => 500,
                    ],
                    'items' => [
                        'label'       => 'Item',
                        'url'         => Backend::url('mit/core/items'),
                        'icon'        => 'icon-cubes',
                        'permissions' => ['mit.core.*'],
                        'order'       => 500,
                    ],
                    'certifications' => [
                        'label'       => 'Certification',
                        'url'         => Backend::url('mit/core/certifications'),
                        'icon'        => 'icon-file',
                        'permissions' => ['mit.core.*'],
                        'order'       => 500,
                    ],
                    'clients' => [
                        'label'       => 'Client',
                        'url'         => Backend::url('mit/core/clients'),
                        'icon'        => 'icon-users',
                        'permissions' => ['mit.core.*'],
                        'order'       => 500,
                    ],
                    'careers' => [
                        'label'       => 'Career',
                        'url'         => Backend::url('mit/core/careers'),
                        'icon'        => 'icon-user-plus',
                        'permissions' => ['mit.core.*'],
                        'order'       => 500,
                    ],
                ]
            ],
        ];
    }
}
